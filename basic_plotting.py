#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt




plt.rc('figure', figsize = [5,3])
plt.rc('xtick', labelsize='Large')
plt.rc('ytick', labelsize='Large')
plt.rc('lines', linewidth = 0.5)
plt.rc('figure', dpi = 300) 



#Read the data
data = pd.read_csv('testdata_Flt63_20190911.csv')



# Plot temperature vertical profile
plt.figure()
plt.plot(data['Temperature[degC]'],data['Altitude [m]']*1e-3)
plt.ylabel('Altitude [km]')
plt.xlabel('Temperature [deg Celsius]')
plt.savefig('Flt63_TempVsAlt.png', dpi = 300)



# Plot ozone vertical profile
plt.figure()
plt.plot(data['Ozone[ppm]'],data['Altitude [m]']*1e-3)
plt.ylabel('Altitude [km]')
plt.xlabel('Ozone [ppm]')
plt.savefig('Flt63_OzoneVsAlt.png')




# Make a nice map
plt.figure()
stamen_terrain = cimgt.Stamen('terrain-background')
ax = plt.axes(projection=stamen_terrain.crs)
# Limit the extent of the map to a small longitude/latitude range.
ax.set_extent([ -71.8, -74,-51,-49.5])
# Add the Stamen data at zoom level 8.
ax.add_image(stamen_terrain, 10)
ax.plot(data['Longitude[deg]'], data['Latitude[deg]'], marker='.', color='b', markersize=1,
             alpha=0.7, transform=ccrs.Geodetic())
plt.savefig('Flt63_Map.png')




# Plot time vs airspeed 
plt.figure()
plt.plot(data['Time[sec]']/60., data['IndicatedAirspeed[m/s]'])
plt.xlabel('Time [min]')
plt.ylabel('Airspeed [kt]')
plt.text(950,85,'Airtow')
plt.savefig('Flt63_TimeVsIAS.png')






