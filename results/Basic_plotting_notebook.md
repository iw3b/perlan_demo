# Perlan Basic Data Processing


```python
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt

plt.rc('figure', figsize = [5,3])
plt.rc('xtick', labelsize='Large')
plt.rc('ytick', labelsize='Large')

%matplotlib inline
```


```python
data = pd.read_csv('testdata_Flt63_20190911.csv')
```


```python
print(data.columns)
```

    Index(['Unnamed: 0', 'Time[sec]', 'Latitude[deg]', 'Longitude[deg]',
           'Altitude [m]', 'IndicatedAirspeed[m/s]', 'DynamicPressure[Pa]',
           'StaticPressure[Pa]', 'Temperature[degC]', 'Ozone[ppm]'],
          dtype='object')


## Some vertical profiles


```python
plt.plot(data['Temperature[degC]'],data['Altitude [m]']*1e-3)
plt.ylabel('Altitude [km]')
plt.xlabel('Temperature [deg Celsius]')
```




    Text(0.5, 0, 'Temperature [deg Celsius]')




![png](output_5_1.png)



```python
plt.plot(data['Ozone[ppm]'],data['Altitude [m]']*1e-3)
plt.ylabel('Altitude [km]')
plt.xlabel('Ozone [ppm]')
```




    Text(0.5, 0, 'Ozone [ppm]')




![png](output_6_1.png)


Apparently there was an instrument failure at ~12.5 km.

## Plots of the Flight


```python
stamen_terrain = cimgt.Stamen('terrain-background')
ax = plt.axes(projection=stamen_terrain.crs)

# Limit the extent of the map to a small longitude/latitude range.
ax.set_extent([ -71.8, -74,-51,-49.5])

# Add the Stamen data at zoom level 8.
ax.add_image(stamen_terrain, 10)

ax.plot(data['Longitude[deg]'], data['Latitude[deg]'], marker='.', color='b', markersize=1,
             alpha=0.7, transform=ccrs.Geodetic())
```




    [<matplotlib.lines.Line2D at 0x12cc2f080>]




![png](output_9_1.png)



```python
plt.plot(data['Time[sec]']/60., data['IndicatedAirspeed[m/s]'])
plt.xlabel('Time [min]')
plt.ylabel('Airspeed [kt]')
plt.text(950,85,'Airtow')
```




    Text(950, 85, 'Airtow')




![png](output_10_1.png)



```python

```


```python

```
